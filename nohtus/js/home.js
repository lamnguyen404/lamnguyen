"use strict";
document.addEventListener('DOMContentLoaded', function() {
    // start function
    var listProduct = $('#carousel-home-product');
    if (listProduct.length > 0) {
        listProduct.owlCarousel({
            loop: true,
            nav: false,
            dots: false,
            autoplay: true,
            mouseDrag: true,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1,
                    margin: 10,
                },
                480: {
                    items: 2,
                    margin: 10,
                },
                992: {
                    items: 3,
                    margin: 20,
                },
                1466: {
                    items: 4,
                    margin: 20,
                }
            },
        });
        $('#carousel-home-product-control .next').click(function () {
            console.log('a');
            listProduct.trigger('next.owl.carousel');
            return false;
        });
        $('#carousel-home-product-control .pre').click(function () {
            console.log('a');
            listProduct.trigger('prev.owl.carousel');
            return false;
        });
    }

    var listReview = $('#carousel-home-review');
    if (listReview.length > 0) {
        listReview.owlCarousel({
            loop: false,
            margin: 0,
            dots: true,
            autoplay: false,
            mouseDrag: true,
            autoplayHoverPause: true,
            items: 1,
        });
    }

    var listKol = $('#carousel-home-kol');
    if (listKol.length > 0) {
        listKol.owlCarousel({
            loop: true,
            margin: 20,
            dots: false,
            autoplay: true,
            mouseDrag: true,
            center:true,
            autoplayHoverPause: true,
            responsive:{
                0:{
                    items:1,
                    stagePadding: 50,
                },
                767:{
                    items:3
                },
                991:{
                    items:4
                },
                1366:{
                    items:6
                },
            }
        });
    }

    // Thêm sự kiện 'click' cho tất cả các nút có class 'scroll-to-target'
    function scrollToTarget(event) {
        var targetId = event.currentTarget.getAttribute('data-target');
        var targetElement = document.getElementById(targetId);
        targetElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
    document.querySelectorAll('.btn-scroll').forEach(button => {
        button.addEventListener("click", scrollToTarget);
    });


    // youtube
    let currentIframe = null;

     // Sự kiện click cho nút "Play"
    $(document).on('click', '.btn-hero-play', function() {
        // Lấy các thuộc tính từ nút "Play"
        var videoId = $(this).data('video');
        var content = $(this).data('content');
        var title = $(this).data('title');
        var subtitle = $(this).data('subtitle');

        // Lấy các phần tử DOM
        var playerDiv = $('#YouTubeBackVideoPlayer');
        var contentDiv = $('#contentModal');
        var titleDiv = $('#titleModal');
        var subtitleDiv = $('#subtitleModal');

        // Xóa nội dung hiện tại và thêm iframe mới
        playerDiv.html(`<iframe id="youtube-player" src="https://www.youtube.com/embed/${videoId}?autoplay=1&enablejsapi=1&loop=1&playlist=${videoId}" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`);
        currentIframe = $('#youtube-player')[0];

        // Đặt nội dung mới cho các phần tử khác
        if (content) contentDiv.html(content);
        if (title) titleDiv.html(title);
        if (subtitle) subtitleDiv.html(subtitle);

    });
    
    $('#heroVideoModal').on('hidden.bs.modal', function () {
        if (currentIframe) {
            // Tạm dừng video bằng cách sử dụng API YouTube
            var command = {
                "event": "command",
                "func": "pauseVideo",
                "args": []
            };
            currentIframe.contentWindow.postMessage(JSON.stringify(command), '*');
        }
    });
});

$(document).ready(function () {
    function initHeroCarousel() {
        var heroNews = $('#carousel-home-hero-news');
        if (heroNews.length > 0) {
            heroNews.owlCarousel({
                loop: true,
                margin: 10,
                dots: false,
                autoplay: true,
                mouseDrag: true,
                center: true,
                autoWidth: true,
                autoplayHoverPause: true,
            });
        }
    }
    
    $(document).ready(function () {
        if ($(window).width() >= 768) {
            $('#carousel-home-hero-news').addClass('off');
        } else {
            initHeroCarousel();
        }
    
        $(window).resize(function () {
            if ($(window).width() >= 768) {
                $('#carousel-home-hero-news').addClass('off');
            } else {
                initHeroCarousel();
            }
        });
    });
    
});

