"use strict";
document.addEventListener("DOMContentLoaded", function() {
 
  // preload
  // var preloader = document.getElementById("preloader");
  // function hidePreloader() {
  //     preloader.classList.add("hidden");
  // }
  // function resourcesLoaded() {
  //   return true;
  // }
  // var checkLoadedInterval = setInterval(function() {
  //   if (resourcesLoaded()) {
  //     clearInterval(checkLoadedInterval);
  //     hidePreloader();
  //   }
  // },100);

  // scroll menu show
  window.addEventListener('load', function() {
    var fsNavbar = document.getElementById("fs-navbar");
  
    if (fsNavbar) {
      var prevScrollpos = window.pageYOffset;
  
      window.onscroll = function() {
        var currentScrollPos = window.pageYOffset;
  
        if (prevScrollpos > currentScrollPos) {
          fsNavbar.style.top = "0";
        } else {
          fsNavbar.style.top = "-81px";
        }
  
        prevScrollpos = currentScrollPos;
      };
    }
  });
  
  // Toggle the side navigation
  $(".js_sidebarToggle, .menu-wrapper-overlay").on('click', function (e) {
    $("body").toggleClass("sidebar-toggled");
    if ($(".menu-wrapper-overlay").length == 0) {
        $("body").append('<div class="menu-wrapper-overlay"  data-toggle="collapse" data-target="#navbarResponsive"></div>');
    } else {
        $(".menu-wrapper-overlay").remove();
    }
  });
  $("body").on('click', '.menu-wrapper-overlay', function (e) {
    $("body").toggleClass("sidebar-toggled");
    $(this).remove();
  });

});